# -*- coding: UTF-8 -*-
import os
fd = open("sensor_node.bin", 'rb')
file_size =  os.path.getsize("sensor_node.bin")
file_data = fd.read(file_size)
fd.close()
body = bytearray()
sum=0
for c in file_data:
    sum += ord(c)
body.extend(file_data)
check_sum=bytearray(4)
check_sum[0] = sum >> 0 & 0xff
check_sum[1] = sum >> 8 & 0xff
check_sum[2] = sum >> 16 & 0xff
check_sum[3] = sum >> 24 & 0xff
body.extend(check_sum)
check_fd=open("smartRF_upgrade.bin","wb")
check_fd.write(body)
check_fd.close()
